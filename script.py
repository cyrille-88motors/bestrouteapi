#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys  
reload(sys)  
sys.setdefaultencoding('utf8')

import sys
import math
import time
import os
import csv
import json

"""
Very simple HTTP server in python.
Usage::
    ./dummy-web-server.py [<port>]
Send a GET request::
    curl http://localhost
Send a HEAD request::
    curl -I http://localhost
Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost
"""
def scoreSameRestaurant(number_of_days_since_last_delivery):
	paramWeight = 1.5
	return max(0., paramWeight * (14. - 0.5 * float(number_of_days_since_last_delivery))) 

def scoreSameCuisine(number_of_days_since_last_delivery_of_same_cuisine_type):
	paramWeight = 0.5
	return max(0, paramWeight * (14 - 0.5 * number_of_days_since_last_delivery_of_same_cuisine_type)) 

def factorIsNewRestaurant(is_new_restaurant):
	if is_new_restaurant == 1:
		return 1.6
	else:
		return 1
		
def returnTotalScoreForLocation(deliveryLocationData):
	#return str(factorIsNewRestaurant(deliveryLocationData['is_new_restaurant'])) + " * (" + str(scoreSameRestaurant(deliveryLocationData['last_order_same_restaurant'])) + " + " + str(scoreSameCuisine(deliveryLocationData['last_order_same_restaurant'])) + ")"
	return factorIsNewRestaurant(deliveryLocationData['is_new_restaurant']) * (scoreSameRestaurant(deliveryLocationData['last_order_same_restaurant']) + scoreSameCuisine(deliveryLocationData['last_order_same_restaurant']))

def returnTotalScoreForRestaurant(restaurantData):
	score = 0
	for deliveryLocation in restaurantData:
		score = score + returnTotalScoreForLocation(restaurantData[deliveryLocation])
	return score


def returnOrderedMatrixRoute(jsonData):	
	data = json.loads(jsonData)
	#data = json.loads('{"Route 1, Klaus": {"KFC Alexa":{"Rocket Internet Mohrenstrasse":{"last_order_same_restaurant":12,"last_order_same_cuisine":22,"is_new_restaurant":0,"order_size":31},"Delivery Hero Pankow":{"last_order_same_restaurant":12,"last_order_same_cuisine":12,"is_new_restaurant":0,"order_size":12}},"Nam Long Sushi":{"Rocket Internet Mohrenstrasse":{"last_order_same_restaurant":6,"last_order_same_cuisine":5,"is_new_restaurant":1,"order_size":31},"Delivery Hero Pankow":{"last_order_same_restaurant":12,"last_order_same_cuisine":12,"is_new_restaurant":1,"order_size":31}}}, "Route 2, Peter": {"KFC Alexa":{"Hitfox":{"last_order_same_restaurant":13,"last_order_same_cuisine":5,"is_new_restaurant":0,"order_size":31},"Ebay":{"last_order_same_restaurant":2,"last_order_same_cuisine":1,"is_new_restaurant":0,"order_size":31}},"Subway Sandwiches":{"Hitfox":{"last_order_same_restaurant":4,"last_order_same_cuisine":1,"is_new_restaurant":0,"is_new_restaurant":0,"order_size":31},"Ebay":{"last_order_same_restaurant":3,"last_order_same_cuisine":3,"is_new_restaurant":0,"order_size":31}}}}')

	matrix = {}
	for route in data:
		matrix[route] = {}
		for restaurant in data[route]:
			matrix[route][restaurant] = returnTotalScoreForRestaurant(data[route][restaurant])

	orderedMatrix = {}
	for route in matrix:
		orderedMatrix[route] = {}
		orderedMatrix[route] = sorted(matrix[route], key=matrix[route].get, reverse=False)
	
	
	return orderedMatrix



from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
		self._set_headers()
		#print (self.path)
		self.wfile.write("")

    def do_HEAD(self):
        self._set_headers()
        
    def do_POST(self):
        content_len = int(self.headers.getheader('content-length'))
        post_body = self.rfile.read(content_len)
        self.send_response(200)
        self.end_headers()

        self._set_headers()
		
   

        self.wfile.write(json.dumps(returnOrderedMatrixRoute(post_body)))
		
        
def run(server_class=HTTPServer, handler_class=S, port=80):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()





