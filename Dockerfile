#to build, do:
#docker build -t bestrouteapi .

#To run:
#docker run -it --rm -p 80:80 bestRouteAPI


FROM jfloff/alpine-python:2.7-slim
COPY ./script.py /home/script.py


EXPOSE 80
CMD ["python", "/home/script.py"]