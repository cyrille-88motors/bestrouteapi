# README #

### Overview ###

This repository allows you to build a HTTP server with docker in order to run an API written in python.

### Technical details: input ###

You should POST with curl the required data in a JSON form. It should be in this format:

```
{
	"Routename":
		{
			"RestoName":
				{
					"DeliveryLocationName":{"last_order_same_restaurant":days_since_last_order_of_same_resto,"last_order_same_cuisine":days_since_last_order_of_same_cuisine_type,"is_new_resto":1_if_yes_0_if_no,"order_size":size_of_the_order}
```

example: 

```
{
	"Route 1, Klaus":
		{
			"KFC Alexa":
				{
					"Rocket Internet Mohrenstrasse":{"last_order_same_restaurant":2,"last_order_same_cuisine":2,"is_new_resto":0,"order_size":31},
					"Delivery Hero Pankow":{"last_order_same_restaurant":2,"last_order_same_cuisine":2,"is_new_resto":0,"order_size":32}
				},
			"Nam Long Sushi":
				{
					"Rocket Internet Mohrenstrasse":{"last_order_same_restaurant":2,"last_order_same_cuisine":2,"is_new_resto":1,"order_size":31},
					"Delivery Hero Pankow":{"last_order_same_restaurant":2,"last_order_same_cuisine":2,"is_new_resto":1,"order_size":32}
				}
		},
	"Route 2, Peter":
		{
			"KFC Alexa":
				{
					"Hitfox":{"last_order_same_restaurant":3,"last_order_same_cuisine":3,"is_new_resto":0,"order_size":13},
					"Ebay":{"last_order_same_restaurant":3,"last_order_same_cuisine":3,"is_new_resto":0,"order_size":19}
				},
			"Subway Sandwiches":
				{
					"Hitfox":{"last_order_same_restaurant":3,"last_order_same_cuisine":3,"is_new_resto":0,"order_size":13},
					"Ebay":{"last_order_same_restaurant":3,"last_order_same_cuisine":3,"is_new_resto":0,"order_size":19}
				}
		}
}
```					
	
### Technical details: output ###

The output is a list of ranked restaurant in JSON format, in the format:
```
{
	"Route1Name": ["restaurant1Name",
```
example:
```
{
	"Route 1, Klaus": 
		[
			"KFC Alexa", 
			"Nam Long Sushi"
		],
	"Route 2, Peter": 
		[
			"KFC Alexa",
			"Subway Sandwiches"
		]
}

```	
	
The higher the name, the better the score.

### Input format ###
- Please note that "last_order_same_restaurant" is always >= last_order_same_cuisine
- You don't need to use the same restaurants for each route, but a restaurant should be present at all delivery locations on one route. 
	
### Step by step set up: Docker image ###

Navigate to the local folder on your terminal, and build your local image with `docker build -t bestrouteapi .`
Then, run the image with `docker run -it --rm -p 80:80 bestrouteapi`


### Step by step set up: test the integration ###

Replace the "LOCAL-IP-OF-YOUR-MACHINE" in the following, and run it: `curl -d '{"Route 1, Klaus": {"KFC Alexa":{"Rocket Internet Mohrenstrasse":{"last_order_same_restaurant":12,"last_order_same_cuisine":22,"is_new_restaurant":0,"order_size":31},"Delivery Hero Pankow":{"last_order_same_restaurant":12,"last_order_same_cuisine":12,"is_new_restaurant":0,"order_size":12}},"Nam Long Sushi":{"Rocket Internet Mohrenstrasse":{"last_order_same_restaurant":6,"last_order_same_cuisine":5,"is_new_restaurant":1,"order_size":31},"Delivery Hero Pankow":{"last_order_same_restaurant":12,"last_order_same_cuisine":12,"is_new_restaurant":1,"order_size":31}}}, "Route 2, Peter": {"KFC Alexa":{"Hitfox":{"last_order_same_restaurant":13,"last_order_same_cuisine":5,"is_new_restaurant":0,"order_size":31},"Ebay":{"last_order_same_restaurant":2,"last_order_same_cuisine":1,"is_new_restaurant":0,"order_size":31}},"Subway Sandwiches":{"Hitfox":{"last_order_same_restaurant":4,"last_order_same_cuisine":1,"is_new_restaurant":0,"is_new_restaurant":0,"order_size":31},"Ebay":{"last_order_same_restaurant":3,"last_order_same_cuisine":3,"is_new_restaurant":0,"order_size":31}}}}' http://LOCAL-IP-OF-YOUR-MACHINE`

It should output something like `{"Route 1, Klaus": ["KFC Alexa", "Nam Long Sushi"], "Route 2, Peter": ["KFC Alexa", "Subway Sandwiches"]}`